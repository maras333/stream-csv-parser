const fs = require('fs');
const split2 = require('split2');
const http = require('http');

const parseCsvStreams = require('./reactiveStreams');
const composedParseCsvStreams = require('./composed');

const main = async () => {
	console.log('\nUsing streams');
	console.log(await parseCsvStreams());

  	console.log('\nUsing composition');
	console.log(await composedParseCsvStreams());
};

main();
