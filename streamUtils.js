const { Transform } = require('stream');

module.exports = {
	parseCSV: (fn, fn2, options = {}) => {
        let keys = [];
        let headline = true;
        return new Transform({
            objectMode: true,
            ...options,

            transform(chunk, encoding, callback) {
                let res;
                try {
                    if (headline) {
                        keys = fn(chunk);
                        headline = false;
                    } else {
                        res = fn2(chunk, keys)
                    }
                } catch (e) {
                    return callback(e);
                }
                callback(null, res ? res : undefined);
            }
        })
    },
	pickFirst10: (options = {}) => {
        let counter = 0;
        return new Transform({
            objectMode: true,
            ...options,

            transform(chunk, encoding, callback) {
                try {
                    if (counter++ < 10) {
                        return callback(null, chunk ? chunk : undefined);
                    }
                    callback(null, null);
                } catch (e) {
                    return callback(e);
                }
            }
        })
    },
	toJSON: (options = {}) => {
        let objects = [];
        return new Transform({
            objectMode: true,
            ...options,

            transform(chunk, encoding, callback) {
                try {
                    objects.push(chunk);
                } catch (e) {
                    return callback(e);
                }
                return callback();
            },

            flush(callback) {
                callback(null, JSON.stringify(objects));
            }
        })
    },

	compose: (...fns) => stream => fns.reduce((t, fn) => t.pipe(fn), stream)
};
