const fs = require('fs');
const split2 = require('split2');
const { parseCSV, pickFirst10, toJSON } = require('./streamUtils');
const { parseToCSV, getKeysFromCsv } = require('./logic');

const reactiveStreams = async () => {
    const stream = await fs.createReadStream('./data/sample.csv');
    parsedCsvStream = stream
        .pipe(split2())
        .pipe(parseCSV(getKeysFromCsv, parseToCSV))
        .pipe(pickFirst10())
        .pipe(toJSON())
        .pipe(process.stdout);
    return new Promise((resolve, reject) => {
        parsedCsvStream.on('data', resolve)
    });
}

module.exports = reactiveStreams;
