const fs = require('fs');
const split2 = require('split2');
const { parseCSV, pickFirst10, toJSON, compose } = require('./streamUtils');
const { parseToCSV, getKeysFromCsv } = require('./logic');

const composedStreams = async () => {
    const stream = await fs.createReadStream('./data/sample.csv');
    parsedCsvStream = compose(
        split2(),
        parseCSV(getKeysFromCsv, parseToCSV),
        pickFirst10(),
        toJSON(),
        process.stdout
    )(stream);
    return new Promise((resolve, reject) => {
        parsedCsvStream.on('data', resolve)
    });
}

module.exports = composedStreams;
