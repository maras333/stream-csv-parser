module.exports = {
	parseToCSV: (row, keys) => {
        const entries = row.toString().split(';');
        const obj = {}
        keys.forEach((key, idx) => obj[key] = entries[idx]);
        return obj;
    },
    getKeysFromCsv: (headline) => {
        let keys = [];
        keys = headline.toString().split(';');
        return keys;
    }
};
